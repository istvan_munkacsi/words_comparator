package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository

import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.WeightsDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import io.reactivex.Completable
import io.reactivex.Maybe
import javax.inject.Inject

class WeightsRepository @Inject constructor(private val weightsDao: WeightsDao) {

    fun insert(weights: WordComparatorNeuralNetwork.WeightsDto): Completable {
        return weightsDao.insert(WordComparatorNeuralNetwork.Weights.fromWeightsDto(weights))
    }

    fun getLast(): Maybe<WordComparatorNeuralNetwork.WeightsDto> {
        return weightsDao.getLast().map { WordComparatorNeuralNetwork.WeightsDto.fromWeights(it) }
    }
}