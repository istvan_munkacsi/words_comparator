package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database

import androidx.annotation.StringRes
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.room.RoomWarnings
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.BaseViewModel
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDb
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbDto
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository.UserDbRepository
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper.compose
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper.io
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class ManageDatabaseViewModel @ViewModelInject constructor(
    private val userDbRepository: UserDbRepository,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(compositeDisposable) {

    sealed class State {
        class ShowSnackbar(@StringRes val message: Int) : State()
        object UserDatabaseAdded : State()
    }

    val state = MutableLiveData<State>()
    val userDbDtoList = MutableLiveData<MutableList<UserDbDto>>()

    init {
        updateUserDbDtoList()
    }

    private fun updateUserDbDtoList() {
        userDbRepository.getAllUserDbDto().io()
            .subscribe { handleUserDbDtoObservable(it) }.compose(this)
    }

    private fun handleUserDbDtoObservable(observable: Observable<UserDbDto>) {
        val userDbDtoList = mutableListOf<UserDbDto>()

        observable.io().doOnComplete {
            this.userDbDtoList.postValue(userDbDtoList)
        }.doOnNext { userDbDto -> userDbDtoList.add(userDbDto) }.subscribe().compose(this)
    }

    fun addUserDb(userDb: UserDb) {
        if (userDb.title.isEmpty()) {
            state.postValue(State.ShowSnackbar(R.string.user_db_add_new_database_title_empty_message))
            return
        }

        userDbRepository.addUserDb(userDb).io().subscribe ({
            state.postValue(State.UserDatabaseAdded)
            updateUserDbDtoList()
        }, {
            if(it.localizedMessage?.contains("code 2067") == true) {
                // UNIQUE Constraint (Index) failed - same title already exists
                state.postValue(State.ShowSnackbar(R.string.user_db_add_new_database_title_already_exists))
            }
        }).compose(this)
    }

    fun removeUserDb(userDbDto: UserDbDto) {
        userDbRepository.removeUserDb(userDbDto.id).io().subscribe { updateUserDbDtoList() }
            .compose(this)
    }

    fun syncUserDbValues(dbId: Int, userDbValues: List<UserDbValue>) {
        userDbRepository.updateUserDbValues(dbId, userDbValues).io().subscribe {
            state.postValue(State.ShowSnackbar(R.string.user_db_synced_message))
            updateUserDbDtoList()
        }.compose(this)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}