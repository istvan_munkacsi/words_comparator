package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room

import android.content.Context
import androidx.room.Room
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.LearnItemDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.UserDbDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.UserDbValueDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.WeightsDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppDatabaseModule {

    @Singleton
    @Provides
    fun providesDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    fun providesUserDbDao(appDatabase: AppDatabase): UserDbDao {
        return appDatabase.userDbDao()
    }

    @Provides
    fun providesUserDbValueDao(appDatabase: AppDatabase): UserDbValueDao {
        return appDatabase.userDbValueDao()
    }

    @Provides
    fun providesLearnItemDao(appDatabase: AppDatabase): LearnItemDao {
        return appDatabase.learnItemDao()
    }

    @Provides
    fun providesWeightsDao(appDatabase: AppDatabase): WeightsDao {
        return appDatabase.weightsDao()
    }
}