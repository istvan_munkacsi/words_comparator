package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.weights

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.BaseViewModel
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository.LearnItemRepository
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository.WeightsRepository
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper.compose
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper.io
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import io.reactivex.disposables.CompositeDisposable

class WeightsViewModel @ViewModelInject constructor(
    compositeDisposable: CompositeDisposable,
    private val learnItemRepository: LearnItemRepository,
    private val weightsRepository: WeightsRepository
) :
    BaseViewModel(compositeDisposable) {

    val learnItems = MutableLiveData<List<WordComparatorNeuralNetwork.LearnItem>>()

    init {
        getLearnItems()
    }

    private fun getLearnItems() {
        learnItemRepository.getAll().io().subscribe({
            learnItems.value = it
        }, {}).compose(this)
    }

    fun delete(learnItem: WordComparatorNeuralNetwork.LearnItem) {
        learnItemRepository.delete(learnItem).io().subscribe {
            getLearnItems()
        }.compose(this)
    }

    fun saveWeights(weights: WordComparatorNeuralNetwork.WeightsDto) {
        weightsRepository.insert(weights).io().subscribe {}.compose(this)
    }
}