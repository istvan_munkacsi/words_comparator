package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper

import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.BaseViewModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun <T> Single<T>.io(): Single<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Maybe<T>.io(): Maybe<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.io(): Observable<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Completable.io(): Completable {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Disposable.compose(parent: BaseViewModel) {
    val compositeDisposableField = BaseViewModel::class.java.getDeclaredField("compositeDisposable").apply {
        this.isAccessible = true
    }

    val compositeDisposable = compositeDisposableField.get(parent) as CompositeDisposable
    compositeDisposable.add(this)
}