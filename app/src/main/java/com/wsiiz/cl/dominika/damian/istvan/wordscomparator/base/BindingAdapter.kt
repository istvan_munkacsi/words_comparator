package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.BaseRecyclerViewAdapter

object BindingAdapter {

    @BindingAdapter(value = [ "items", "concatAdapter", "adapterForItemsIndex"], requireAll = false)
    @JvmStatic
    fun <Items> setItems(
        recyclerView: RecyclerView,
        items: List<Items>?,
        concatAdapter: ConcatAdapter,
        adapterForItemsIndex: Int = 0
    ) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = concatAdapter
        }

        if (items != null) {
            val adapter = (recyclerView.adapter as ConcatAdapter).adapters[adapterForItemsIndex]
            (adapter as BaseRecyclerViewAdapter<*, Items>).setupItems(items)
        }
    }
}