package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.control

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.databinding.PredictionViewBinding

class PredictionView(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private var binding: PredictionViewBinding

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.prediction_view, this, true)
        visibility = View.GONE
    }

    fun <Item> examineData(
        items: MutableList<Item>,
        onSubmit: (Item) -> Unit,
        onNo: (Item) -> Unit,
        onFinished: () -> Unit = {},
        getTitle: (forItem: Item) -> String,
        hideWhenFirstSelected: Boolean
    ) {
        if (items.isEmpty()) {
            visibility = View.GONE
            onFinished()
            return
        }
        visibility = View.VISIBLE
        binding.titleView.text = getTitle(items[0])
        binding.noActionButton.setOnClickListener {
            onNo(items[0])

            examineData(
                items.apply { removeAt(0) },
                onSubmit,
                onNo,
                onFinished,
                getTitle,
                hideWhenFirstSelected
            )
        }

        binding.yesActionButton.setOnClickListener {
            if (hideWhenFirstSelected) visibility = View.GONE
            onSubmit(items[0])
            if (!hideWhenFirstSelected) examineData(
                items.apply { removeAt(0) },
                onSubmit,
                onNo,
                onFinished,
                getTitle,
                hideWhenFirstSelected
            )
        }
    }
}