package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model

data class CalculationResultDto(
    val userDb: UserDbDto,
    val userDbValue: UserDbValue,
    val result: Float
)