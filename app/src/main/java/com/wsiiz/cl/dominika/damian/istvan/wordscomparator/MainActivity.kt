package com.wsiiz.cl.dominika.damian.istvan.wordscomparator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.databinding.MainActivityBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
    }

    fun navigateTo(destinationId: Int) {
        binding.navHostFragment.findNavController().navigate(destinationId)
    }

    fun popBackStack() {
        binding.navHostFragment.findNavController().popBackStack()
    }
}