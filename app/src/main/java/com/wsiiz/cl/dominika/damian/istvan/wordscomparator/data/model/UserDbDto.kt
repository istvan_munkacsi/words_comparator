package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model

data class UserDbDto(
    val id: Int,
    val title: String,
    var values: List<UserDbValue>
)