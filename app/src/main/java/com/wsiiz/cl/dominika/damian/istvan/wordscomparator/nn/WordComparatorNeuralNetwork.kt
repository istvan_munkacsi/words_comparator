package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn

import android.annotation.SuppressLint
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import kotlin.random.Random

@SuppressLint("DefaultLocale")
class WordComparatorNeuralNetwork {

    companion object {
        private const val MIN_POSSIBLE_SUB_WORD_LENGTH = 2
        private const val WEIGHTS_SEARCH_MAX_EPOCH_COUNT = 1000000

        data class ProcessedWord(
            var word: String = "",
            var subWords: List<String> = listOf()
        )

        fun separateToSubWords(query: String): List<ProcessedWord> {
            val processedWords = mutableListOf<ProcessedWord>()
            val queryByWords = query.toLowerCase().split(" ")

            for (word in queryByWords) {
                val processedWord =
                    ProcessedWord(
                        word = word
                    )
                val subWords = mutableListOf<String>()
                val wordLength = word.length

                for (i in 0 until wordLength) {
                    var j = wordLength

                    while (i != j && j - i >= MIN_POSSIBLE_SUB_WORD_LENGTH) {
                        val subWord = word.substring(i, j)
                        subWords.add(subWord)

                        j--
                    }
                }

                processedWord.subWords = subWords
                processedWords.add(processedWord)
            }
            return processedWords
        }

        fun calculatePoints(processedWords: List<ProcessedWord>): Int {
            var sum = 0

            for (processedWord in processedWords) {
                for (subWord in processedWord.subWords) {
                    val subWordLength = subWord.length
                    sum += subWordLength
                }
            }

            return sum
        }

        private fun calculateWordsPoints(words: List<String>): Int {
            var sum = 0

            for (word in words) {
                val wordLength = word.length
                sum += wordLength
            }

            return sum
        }

        private fun <T> innerJoin(list1: List<T>, list2: List<T>): List<T> {
            val commonItems = mutableListOf<T>()

            for (subItem1 in list1) {
                for (subItem2 in list2) {
                    if (
                        subItem1 == subItem2 ||
                        (subItem1 is String && subItem2 is String && subItem1.equals(subItem2, ignoreCase = true))
                    ) {
                        commonItems.add(subItem1)
                    }
                }
            }
            return commonItems
        }

        fun findWeights(learnItems: List<LearnItem>): WeightsDto {
            var weightsList = listOf<Float>()
            var bias = 0f
            var error = 0

            for (i in 0 until WEIGHTS_SEARCH_MAX_EPOCH_COUNT) {
                val currWeightsList = mutableListOf<Float>()
                for (wi in 0 until 3) {
                    currWeightsList.add((Random.nextFloat() + 0.01f) * 2 - 1f)
                }
                val currBias = (Random.nextFloat() + 0.01f) * 20f - 10
                val currWeights =
                    WeightsDto(
                        currWeightsList,
                        currBias
                    )
                var currError = 0


                for (learnItem in learnItems) {
                    val firstWordSimilarityScore =
                        calculateScore(
                            learnItem.query,
                            learnItem.firstWord,
                            currWeights
                        )
                    val secondWordSimilarityScore =
                        calculateScore(
                            learnItem.query,
                            learnItem.secondWord,
                            currWeights
                        )

                    if (secondWordSimilarityScore >= firstWordSimilarityScore) {
                        currError++
                    }
                }
                if (currError < error || error == 0) {
                    weightsList = currWeightsList
                    bias = currBias
                    error = currError
                }

                if (error == 0) {
                    break
                }
            }

            return WeightsDto(
                weightsList,
                bias
            )
        }

        fun calculateScore(
            userDbValue: UserDbValue,
            toSolveDbValue: ToSolveDbValue,
            weights: WeightsDto? = null
        ): Float {
            return calculateScore(
                userDbValue.value,
                toSolveDbValue,
                weights
            )
        }

        fun calculateScore(
            toSolveWordString: String,
            secondWordString: String,
            weights: WeightsDto?
        ): Float {
            val toSolveWordSubWords =
                separateToSubWords(
                    toSolveWordString
                )
            val toSolveWordPoints =
                calculatePoints(
                    toSolveWordSubWords
                )
            val toSolveWordWords = toSolveWordString.split(" ")
            val toSolveWordToSolveDbValue =
                ToSolveDbValue(
                    toSolveWordPoints,
                    toSolveWordSubWords,
                    toSolveWordWords
                )

            return calculateScore(
                secondWordString,
                toSolveWordToSolveDbValue,
                weights
            )
        }

        fun calculateScore(
            firstWordString: String,
            secondWordToSolveDbValue: ToSolveDbValue,
            weights: WeightsDto?
        ): Float {
            val dbValueSubWords =
                separateToSubWords(
                    firstWordString
                )
            val dbValueSubWordsPoints =
                calculatePoints(
                    dbValueSubWords
                )
            val avgPoints = (dbValueSubWordsPoints + secondWordToSolveDbValue.points) / 2
            val commonSubWords =
                innerJoin(
                    dbValueSubWords,
                    secondWordToSolveDbValue.subWords
                )

            val commonSubWordsPoints =
                calculatePoints(
                    commonSubWords
                )

            val userDbValueSeparatedWords = firstWordString.split(" ")
            val commonWords =
                innerJoin(
                    userDbValueSeparatedWords,
                    secondWordToSolveDbValue.separatedWords
                )

            val commonWordsPoints =
                calculateWordsPoints(
                    commonWords
                )
            val commonWordsPercentage =
                1 + (commonWords.size.toFloat() / (userDbValueSeparatedWords.size + secondWordToSolveDbValue.separatedWords.size) * commonWordsPoints)

            return if (weights == null || weights.weights.isEmpty()) {
                similarity(
                    avgPoints,
                    commonSubWordsPoints * commonWordsPercentage
                )
            } else {
                val weight1 = weights.weights[0]
                val weight2 = weights.weights[1]
                val weight3 = weights.weights[2]
                val bias = weights.bias
                similarity(
                    avgPoints,
                    weight1 * commonSubWordsPoints + weight2 * commonWordsPoints + weight3 * commonWordsPercentage + bias
                )
            }
        }

        private fun similarity(totalPoints: Float, scoredPoints: Float) = scoredPoints / totalPoints
        private fun similarity(totalPoints: Int, scoredPoints: Float) =
            similarity(
                totalPoints.toFloat(),
                scoredPoints
            )
    }

    data class ToSolveDbValue(
        val points: Int,
        val subWords: List<ProcessedWord>,
        val separatedWords: List<String>
    )

    @Entity
    data class LearnItem(
        val firstWord: String,
        val secondWord: String,
        val query: String,

        @PrimaryKey(autoGenerate = true)
        val id: Int = 0
    )

    data class WeightsDto(
        val weights: List<Float>,
        val bias: Float
    ) {
        companion object {
            fun fromWeights(weights: Weights) =
                WeightsDto(listOf(weights.weight1, weights.weight2, weights.weight3), weights.bias)
        }
    }

    @Entity
    data class Weights(
        val weight1: Float,
        val weight2: Float,
        val weight3: Float,
        val bias: Float,

        @PrimaryKey
        val id: Int = 1
    ) {
        companion object {
            fun fromWeightsDto(weightsDto: WeightsDto) = Weights(
                weightsDto.weights[0],
                weightsDto.weights[1],
                weightsDto.weights[2],
                weightsDto.bias
            )
        }
    }
}
