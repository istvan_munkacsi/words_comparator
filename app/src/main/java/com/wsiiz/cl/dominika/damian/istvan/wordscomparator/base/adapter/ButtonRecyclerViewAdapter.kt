package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R

open class ButtonRecyclerViewAdapter(
    @LayoutRes private val layoutId: Int,
    buttonItems: List<ButtonInfo>
) : BaseRecyclerViewAdapter<ButtonRecyclerViewAdapter.ButtonViewHolder, ButtonRecyclerViewAdapter.ButtonInfo>() {

    init {
        this.items = buttonItems
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ButtonViewHolder {
        return ButtonViewHolder(inflateView(parent))
    }

    protected fun inflateView(parent: ViewGroup): View {
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
    }

    override fun onBindViewHolder(holder: ButtonViewHolder, position: Int) {
        val buttonInfo = items[position]
        holder.actionButton.apply {
            if (buttonInfo.title != null) {
                setText(buttonInfo.title)
            }

            setOnClickListener { buttonInfo.onClick(getArguments(holder)) }
        }
    }

    protected open fun getArguments(viewHolder: ButtonViewHolder): Bundle {
        return bundleOf()
    }

    open class ButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val actionButton: TextView = view.findViewById(R.id.actionButton)
    }


    data class ButtonInfo(
        @StringRes
        val title: Int? = null,
        val onClick: (arguments: Bundle) -> Unit
    )
}