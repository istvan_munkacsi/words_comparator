package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository

import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.LearnItemDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class LearnItemRepository @Inject constructor(private val learnItemDao: LearnItemDao) {

    fun insert(learnItem: WordComparatorNeuralNetwork.LearnItem): Completable {
        return learnItemDao.insert(learnItem)
    }

    fun delete(learnItem: WordComparatorNeuralNetwork.LearnItem): Completable {
        return learnItemDao.delete(learnItem)
    }

    fun getAll(): Single<List<WordComparatorNeuralNetwork.LearnItem>> {
        return learnItemDao.getAll()
    }
}