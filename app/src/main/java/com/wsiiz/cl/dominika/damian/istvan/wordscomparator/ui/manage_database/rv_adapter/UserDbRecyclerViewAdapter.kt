package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database.rv_adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.BaseRecyclerViewAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbDto
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue

class UserDbRecyclerViewAdapter(private val interactionContract: InteractionContract) :
    BaseRecyclerViewAdapter<UserDbRecyclerViewAdapter.UserDbViewHolder, UserDbDto>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserDbViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_db_recycler_view_item, parent, false)

        return UserDbViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: UserDbViewHolder, position: Int) {
        val userDB = items[position]

        holder.apply {
            textInputLayout.hint = userDB.title
            textInputEditText.apply {
                setText(userDB.values.joinToString(separator = ", ") { it.value })
                setSelection(text.length)
            }

            syncButton.setOnClickListener {
                val newValues = mutableListOf<UserDbValue>()
                val newValuesValues = textInputEditText.text.toString().split(",")
                for (newValueValue in newValuesValues) {
                    val userDbValue = UserDbValue(value = newValueValue.trim(), userDbId = userDB.id)
                    newValues.add(userDbValue)
                }
                interactionContract.onSyncClick(userDB.id, newValues)
                textInputEditText.clearFocus()
            }
            deleteButton.setOnClickListener { interactionContract.onDeleteClick(userDB) }
        }
    }

    override fun getDiffUtilCallback(newItems: List<UserDbDto>, oldItems: List<UserDbDto>) =
        UserDatabaseRecyclerViewAdapterDiffCallback(newItems, oldItems)

    class UserDbViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textInputEditText: EditText = view.findViewById(R.id.textInputEditText)
        val syncButton: View = view.findViewById(R.id.syncButton)
        val deleteButton: View = view.findViewById(R.id.deleteButton)
        val textInputLayout: TextInputLayout = view.findViewById(R.id.textInputLayout)
    }

    interface InteractionContract {
        fun onSyncClick(dbId: Int, userDbValues: List<UserDbValue>)
        fun onDeleteClick(userDbDto: UserDbDto)
    }
}