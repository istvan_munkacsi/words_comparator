package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.main

import android.annotation.SuppressLint
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.BaseViewModel
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.*
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository.LearnItemRepository
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository.UserDbRepository
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository.WeightsRepository
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper.compose
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.helper.io
import io.reactivex.Maybe
import io.reactivex.disposables.CompositeDisposable

@SuppressLint("DefaultLocale")
class MainViewModel @ViewModelInject constructor(
    compositeDisposable: CompositeDisposable,
    private val userDbRepository: UserDbRepository,
    private val learnItemRepository: LearnItemRepository,
    private val weightsRepository: WeightsRepository
) : BaseViewModel(compositeDisposable) {

    sealed class State {
        object Initial : State()
        object Calculating : State()
        class Calculated(
            val word: String,
            val bestScores: List<ValueScore>,
            val scoresByDb: List<DbScore>
        ) : State()
    }

    data class ValueScore(
        val dbValue: UserDbValue,
        val score: Float
    )

    data class DbScore(
        val userDbDto: UserDbDto,
        val score: Float
    ) {

        override fun toString() = userDbDto.title
    }

    lateinit var mainFragmentNavigator: MainFragmentNavigatorContract
    val state = MutableLiveData<State>()

    fun findMostSimilarWords(word: String) {
        weightsRepository.getLast().io().doOnEvent { t1, t2 ->
            if(t1 == null) {
                state.value = State.Calculating

                val userDbDtoList = mutableListOf<UserDbDto>()

                userDbRepository.getAllUserDbDto().io().subscribe {
                    it.io()
                        .doOnComplete {
                            val results = findMostSimilarWords(word, userDbDtoList, null)
                            showOutputResults(word, results)
                        }
                        .doOnNext { userDbDto -> userDbDtoList.add(userDbDto) }
                        .subscribe()
                        .compose(this)
                }.compose(this)
            }
        }.subscribe { weights ->
            state.value = State.Calculating

            val userDbDtoList = mutableListOf<UserDbDto>()

            userDbRepository.getAllUserDbDto().io().subscribe {
                it.io()
                    .doOnComplete {
                        val results = findMostSimilarWords(word, userDbDtoList, weights)
                        showOutputResults(word, results)
                    }
                    .doOnNext { userDbDto -> userDbDtoList.add(userDbDto) }
                    .subscribe()
                    .compose(this)
            }.compose(this)
        }.compose(this)
    }

    private fun findMostSimilarWords(
        toSolveQuery: String,
        userDbDtoList: List<UserDbDto>,
        weights: WordComparatorNeuralNetwork.WeightsDto?
    ): List<CalculationResultDto> {
        val result = mutableListOf<CalculationResultDto>()

        val toSolveSubWords = WordComparatorNeuralNetwork.separateToSubWords(toSolveQuery)
        val toSolvePoints = WordComparatorNeuralNetwork.calculatePoints(toSolveSubWords)
        val toSolveSeparatedWords = toSolveQuery.split(" ")
        val toSolveDbValue = WordComparatorNeuralNetwork.ToSolveDbValue(
            toSolvePoints,
            toSolveSubWords,
            toSolveSeparatedWords
        )

        for (userDbDto in userDbDtoList) {
            for (userDbValue in userDbDto.values) {
                val similarityScore =
                    WordComparatorNeuralNetwork.calculateScore(userDbValue, toSolveDbValue, weights)
                val calculationResultDto =
                    CalculationResultDto(userDbDto, userDbValue, similarityScore)

                result.add(calculationResultDto)
            }
        }

        return result
    }

    private fun showOutputResults(word: String, results: List<CalculationResultDto>) {
        val scoresByDb = mutableMapOf<UserDbDto, Float>()
        for (result in results) {
            scoresByDb[result.userDb] = (scoresByDb[result.userDb] ?: 0f) + result.result
        }

        val dbScoreList = mutableListOf<DbScore>()
        for (entry in scoresByDb) {
            val dbScore = DbScore(entry.key, entry.value)
            dbScoreList.add(dbScore)
        }

        val bestScores =
            results.map { ValueScore(it.userDbValue, it.result) }.sortedBy { it.score }.reversed()
        state.value = State.Calculated(
            word, bestScores, dbScoreList.sortedBy { it.score }.reversed()
        )
    }

    fun onCalculationFinished() {
        state.value = State.Initial
    }

    interface MainFragmentNavigatorContract {
        fun showManageDatabaseFragment()
        fun showWeightsFragment()
    }

    fun addDbValue(dbValue: UserDbValue) {
        userDbRepository.addDbValue(dbValue).io().subscribe {
            state.value = State.Initial
        }.compose(this)
    }

    fun insert(learnItem: WordComparatorNeuralNetwork.LearnItem) {
        learnItemRepository.insert(learnItem).io().subscribe {}.compose(this)
    }
}