package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<ViewHolder : RecyclerView.ViewHolder, Items> :
    RecyclerView.Adapter<ViewHolder>() {

    protected var items = listOf<Items>()

    @Synchronized
    open fun setupItems(items: List<Items>) {
        val diffResult = DiffUtil.calculateDiff(getDiffUtilCallback(items, this.items))
        this.items = items

        diffResult.dispatchUpdatesTo(this)
    }

    protected open fun getDiffUtilCallback(newItems: List<Items>, oldItems: List<Items>): DiffUtil.Callback {
        return BaseDiffUtilCallback(newItems, oldItems)
    }

    override fun getItemCount() = items.count()
}