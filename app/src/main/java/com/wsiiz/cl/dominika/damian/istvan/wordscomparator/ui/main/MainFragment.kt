package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.MainActivity
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.toWordsPairList
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.databinding.MainFragmentLayoutBinding
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment(), MainViewModel.MainFragmentNavigatorContract {

    companion object {
        const val MAX_BEST_SCORES_VARIANTS_IN_OUTPUT = 5
        const val RESULT_THRESHOLD = 0f
    }

    private lateinit var binding: MainFragmentLayoutBinding
    private val viewModel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentLayoutBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.vm = viewModel
        viewModel.mainFragmentNavigator = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is MainViewModel.State.Calculating -> {
                    binding.predictionView.visibility = View.GONE
                    binding.outputView.text = getString(R.string.main_fragment_calculating)
                }
                is MainViewModel.State.Calculated -> {
                    var output = ""
                    for (i in state.bestScores.indices) {
                        if (i > MAX_BEST_SCORES_VARIANTS_IN_OUTPUT) break
                        if (state.bestScores[i].score <= RESULT_THRESHOLD) break

                        if (output.isEmpty()) output += getString(R.string.main_fragment_top_similarity_words) + "\n"

                        output += String.format(
                            getString(R.string.main_fragment_best_score),
                            i + 1,
                            state.bestScores[i].dbValue.value
                        )
                    }

                    binding.outputView.text =
                        if (output.isEmpty()) getString(R.string.main_fragment_output_empty) else output

                    binding.predictionView.examineData(
                        state.scoresByDb.toMutableList(),
                        onSubmit = { dbScore ->
                            val dbValue = UserDbValue(
                                value = state.word,
                                userDbId = dbScore.userDbDto.id
                            )

                            viewModel.addDbValue(dbValue)

                            val wordsPairList =
                                dbScore.userDbDto.values.map { it.value }.toWordsPairList()

                            binding.predictionView.examineData(
                                wordsPairList.toMutableList(),
                                onSubmit = { wordsPair ->
                                    val learnItem = WordComparatorNeuralNetwork.LearnItem(
                                        wordsPair.value1,
                                        wordsPair.value2,
                                        state.word
                                    )
                                    viewModel.insert(learnItem)
                                },
                                onFinished = {
                                    viewModel.onCalculationFinished()
                                }, getTitle = { wordsPair ->
                                    String.format(
                                        getString(R.string.main_fragment_is_word_more_similar),
                                        state.word,
                                        wordsPair.value1,
                                        wordsPair.value2
                                    )
                                }, hideWhenFirstSelected = false,
                                onNo = { wordsPair ->
                                    val learnItem = WordComparatorNeuralNetwork.LearnItem(
                                        wordsPair.value2,
                                        wordsPair.value1,
                                        state.word
                                    )
                                    viewModel.insert(learnItem)
                                }
                            )
                        },
                        onFinished = {
                            viewModel.onCalculationFinished()
                        }, getTitle = { dbScore ->
                            String.format(
                                getString(R.string.main_fragment_belongs_to),
                                state.word,
                                dbScore.userDbDto.title
                            )
                        }, hideWhenFirstSelected = true, onNo = { wordsPair ->

                        })
                }
                is MainViewModel.State.Initial -> {
                    binding.outputView.text = ""
                    binding.searchWordView.setText("")
                }
            }
        })
    }

    override fun showManageDatabaseFragment() {
        (activity as MainActivity?)?.navigateTo(R.id.action_mainFragment_to_manageDatabase)
    }

    override fun showWeightsFragment() {
        (activity as MainActivity?)?.navigateTo(R.id.action_mainFragment_to_weightsFragment)
    }
}