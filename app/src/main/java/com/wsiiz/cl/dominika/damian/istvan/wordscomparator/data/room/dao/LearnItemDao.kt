package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface LearnItemDao {

    @Insert
    fun insert(learnItem: WordComparatorNeuralNetwork.LearnItem): Completable

    @Delete
    fun delete(learnItem: WordComparatorNeuralNetwork.LearnItem): Completable

    @Query("SELECT * FROM learnitem")
    fun getAll(): Single<List<WordComparatorNeuralNetwork.LearnItem>>
}