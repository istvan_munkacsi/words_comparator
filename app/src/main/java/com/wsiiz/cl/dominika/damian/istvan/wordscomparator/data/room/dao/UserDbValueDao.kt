package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao

import androidx.room.*
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDb
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class UserDbValueDao {

    @Query("SELECT * FROM userdbvalue WHERE userDbId = :dbId")
    abstract fun getByUserDbId(dbId: Int): Maybe<List<UserDbValue>>

    @Query("DELETE FROM userdbvalue WHERE userDbId = :dbId")
    abstract fun deleteByUserDbId(dbId: Int): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrUpdateUserDbValue(userDbValues: List<UserDbValue>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOnUpdate(dbValue: UserDbValue): Completable
}