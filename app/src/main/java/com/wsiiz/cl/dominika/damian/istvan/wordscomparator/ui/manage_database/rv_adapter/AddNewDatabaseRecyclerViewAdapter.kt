package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database.rv_adapter

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.ButtonRecyclerViewAdapter

class AddNewDatabaseRecyclerViewAdapter(items: List<ButtonInfo>) : ButtonRecyclerViewAdapter(
    R.layout.manage_database_add_database_recycler_view_item_layout, items
) {

    companion object {
        const val DATABASE_TITLE_ARG = "database_title"
    }

    private var clearTitle: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ButtonViewHolder {
        return AddDatabaseViewHolder(
            inflateView(parent)
        )
    }

    override fun onBindViewHolder(holder: ButtonViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        holder as AddDatabaseViewHolder
        if(clearTitle) {
            clearTitle = false
            holder.databaseTitleView.text = ""
        }
    }

    fun clearTitle() {
        clearTitle = true
        notifyItemChanged(0)
    }

    class AddDatabaseViewHolder(view: View) : ButtonViewHolder(view) {
        val databaseTitleView: TextView = view.findViewById(R.id.databaseTitleView)
    }

    override fun getArguments(viewHolder: ButtonViewHolder): Bundle {
        viewHolder as AddDatabaseViewHolder

        return bundleOf(
            Pair(DATABASE_TITLE_ARG, viewHolder.databaseTitleView.text.toString())
        )
    }
}