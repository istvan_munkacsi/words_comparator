package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.AppDatabase.Companion.DATABASE_VERSION
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.UserDbDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDb
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.LearnItemDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.UserDbValueDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.WeightsDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork

@Database(
    entities = [UserDb::class, UserDbValue::class, WordComparatorNeuralNetwork.LearnItem::class, WordComparatorNeuralNetwork.Weights::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "wordscomparator_db"
    }

    abstract fun userDbDao(): UserDbDao
    abstract fun userDbValueDao(): UserDbValueDao
    abstract fun learnItemDao(): LearnItemDao
    abstract fun weightsDao(): WeightsDao
}