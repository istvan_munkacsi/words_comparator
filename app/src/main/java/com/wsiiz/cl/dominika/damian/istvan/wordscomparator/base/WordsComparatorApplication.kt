package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WordsComparatorApplication : Application() {
}