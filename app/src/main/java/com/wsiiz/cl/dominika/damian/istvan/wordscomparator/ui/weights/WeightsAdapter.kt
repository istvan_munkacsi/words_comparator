package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.weights

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.BaseRecyclerViewAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork

class WeightsAdapter(private val interactionContract: WeightsAdapterInteractionContract) :
    BaseRecyclerViewAdapter<WeightsAdapter.WeightsViewHolder, WordComparatorNeuralNetwork.LearnItem>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeightsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.weights_recycler_view_item_layout, parent, false)
        return WeightsViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeightsViewHolder, position: Int) {
        val item = items[position]

        holder.apply {
            titleView.text = String.format(
                itemView.context.getString(R.string.weights_fragment_rv_item),
                item.query,
                item.firstWord,
                item.secondWord
            )
            removeView.setOnClickListener {
                interactionContract.onLearnItemRemoveClick(item)
            }
        }
    }

    class WeightsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleView: TextView = view.findViewById(R.id.titleView)
        val removeView: View = view.findViewById(R.id.removeView)
    }

    interface WeightsAdapterInteractionContract {
        fun onLearnItemRemoveClick(item: WordComparatorNeuralNetwork.LearnItem)
    }
}