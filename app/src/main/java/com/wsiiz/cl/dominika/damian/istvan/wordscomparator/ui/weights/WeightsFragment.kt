package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.weights

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.MainActivity
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.R
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.ButtonRecyclerViewAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.databinding.WeightsFragmentLayoutBinding
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeightsFragment : Fragment(), WeightsAdapter.WeightsAdapterInteractionContract {

    private lateinit var binding: WeightsFragmentLayoutBinding
    private val viewModel by viewModels<WeightsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = WeightsFragmentLayoutBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.vm = viewModel
        binding.adapter = getRvAdapter()


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    (activity as MainActivity?)?.popBackStack()
                }
            })
    }

    private fun getRvAdapter(): ConcatAdapter {
        val weightsAdapter = WeightsAdapter(this)
        val buttonAdapter = ButtonRecyclerViewAdapter(
            R.layout.calculate_weights_button_item, listOf(
                ButtonRecyclerViewAdapter.ButtonInfo(onClick = { startWeightsCalculation() })
            )
        )
        return ConcatAdapter(weightsAdapter, buttonAdapter)
    }

    override fun onLearnItemRemoveClick(item: WordComparatorNeuralNetwork.LearnItem) {
        viewModel.delete(item)
    }

    private fun startWeightsCalculation() {
        binding.loadingWeights.visibility = View.VISIBLE

        viewModel.learnItems.value?.let {
            val weights = WordComparatorNeuralNetwork.findWeights(it)
            binding.weightsView.text = String.format(
                getString(R.string.weights_fragment_weights),
                weights.weights[0],
                weights.weights[1],
                weights.weights[2],
                weights.bias
            )
            binding.weightsView.visibility = View.VISIBLE
            binding.loadingWeights.visibility = View.GONE
            viewModel.saveWeights(weights)
        }
    }
}