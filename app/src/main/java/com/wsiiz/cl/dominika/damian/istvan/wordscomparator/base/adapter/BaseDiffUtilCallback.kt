package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter

import androidx.recyclerview.widget.DiffUtil

open class BaseDiffUtilCallback <Items>(private val newItems: List<Items>, private val oldItems: List<Items>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]

        return oldItem == newItem
    }

    override fun getOldListSize() = oldItems.size

    override fun getNewListSize() = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areItemsTheSame(oldItemPosition, newItemPosition)
    }
}