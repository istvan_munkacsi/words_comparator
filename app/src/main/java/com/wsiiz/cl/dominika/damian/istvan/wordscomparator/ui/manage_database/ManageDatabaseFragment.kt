package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ConcatAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.MainActivity
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.ButtonRecyclerViewAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDb
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbDto
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.databinding.ManageDatabaseFragmentBinding
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database.rv_adapter.AddNewDatabaseRecyclerViewAdapter
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database.rv_adapter.UserDbRecyclerViewAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ManageDatabaseFragment : Fragment(), UserDbRecyclerViewAdapter.InteractionContract {

    private lateinit var binding: ManageDatabaseFragmentBinding
    private val viewModel: ManageDatabaseViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ManageDatabaseFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.vm = viewModel
        binding.adapter = buildRecyclerViewAdapter()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is ManageDatabaseViewModel.State.ShowSnackbar -> {
                    val context = context ?: return@Observer
                    Toast.makeText(context, getString(state.message), Toast.LENGTH_LONG).show()
                }
                is ManageDatabaseViewModel.State.UserDatabaseAdded -> {
                    addDatabaseAdapter()?.clearTitle()
                }
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity?)?.popBackStack()
            }
        })
    }

    private fun addDatabaseAdapter() =
        binding.adapter?.adapters?.get(1) as AddNewDatabaseRecyclerViewAdapter?

    private fun buildRecyclerViewAdapter(): ConcatAdapter {
        // UserDbRecyclerViewAdapter
        val userDbRecyclerViewAdapter =
            UserDbRecyclerViewAdapter(this)

        // AddNewDatabaseAdapter
        val buttonItems = listOf(
            ButtonRecyclerViewAdapter.ButtonInfo(onClick = { args ->
                val dbTitle = args.getString(AddNewDatabaseRecyclerViewAdapter.DATABASE_TITLE_ARG)
                    ?: return@ButtonInfo
                addNewUserDb(dbTitle)
            })
        )
        val addNewDatabaseAdapter =
            AddNewDatabaseRecyclerViewAdapter(
                buttonItems
            )

        // ConcatAdapter
        return ConcatAdapter(userDbRecyclerViewAdapter, addNewDatabaseAdapter)
    }

    override fun onSyncClick(dbId: Int, userDbValues: List<UserDbValue>) {
        viewModel.syncUserDbValues(dbId, userDbValues)
    }

    override fun onDeleteClick(userDbDto: UserDbDto) {
        viewModel.removeUserDb(userDbDto)
    }

    private fun addNewUserDb(title: String) {
        val userDb = UserDb(title = title)
        viewModel.addUserDb(userDb)
    }
}