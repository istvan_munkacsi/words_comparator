package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.ui.manage_database.rv_adapter

import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base.adapter.BaseDiffUtilCallback
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbDto

class UserDatabaseRecyclerViewAdapterDiffCallback(
    private val newItems: List<UserDbDto>,
    private val oldItems: List<UserDbDto>
) : BaseDiffUtilCallback<UserDbDto>(newItems, oldItems) {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]

        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return false
    }
}