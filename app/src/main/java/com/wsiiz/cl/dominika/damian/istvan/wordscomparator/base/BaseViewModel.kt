package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(protected val compositeDisposable: CompositeDisposable) : ViewModel() {
}