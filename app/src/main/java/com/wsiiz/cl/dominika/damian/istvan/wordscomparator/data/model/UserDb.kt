package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["title"], unique = true)])
data class UserDb(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String
)