package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.repository

import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDb
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbDto
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model.UserDbValue
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.UserDbDao
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao.UserDbValueDao
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import javax.inject.Inject

class UserDbRepository @Inject constructor(
    private val userDbDao: UserDbDao,
    private val userDbValueDao: UserDbValueDao
) {

    fun getAllUserDbDto(): Maybe<Observable<UserDbDto>> {
        return userDbDao.getAllUserDb().map {
            Observable.fromIterable(it).flatMap { userDb ->
                return@flatMap userDbValueDao.getByUserDbId(userDb.id).map { userDbValues ->
                    UserDbDto(userDb.id, userDb.title, userDbValues)
                }.toObservable()
            }
        }
    }

    fun addUserDb(userDb: UserDb): Completable {
        return userDbDao.addUserDb(userDb)
    }

    fun removeUserDb(id: Int): Completable {
        return userDbDao.removeUserDb(id)
    }

    fun updateUserDbValues(dbId: Int, userDbValues: List<UserDbValue>): Completable {
        return userDbValueDao.deleteByUserDbId(dbId)
            .concatWith(userDbValueDao.insertOrUpdateUserDbValue(userDbValues))

    }

    fun addDbValue(dbValue: UserDbValue): Completable {
        return userDbValueDao.insertOnUpdate(dbValue)
    }
}