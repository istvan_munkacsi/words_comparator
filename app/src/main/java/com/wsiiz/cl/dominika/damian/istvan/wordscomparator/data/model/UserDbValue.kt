package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["value", "userDbId"], unique = true)])
data class UserDbValue(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val value: String,

    @ForeignKey(entity = UserDb::class, parentColumns = ["id"], childColumns = ["userDbId"], onDelete = ForeignKey.CASCADE)
    val userDbId: Int
)