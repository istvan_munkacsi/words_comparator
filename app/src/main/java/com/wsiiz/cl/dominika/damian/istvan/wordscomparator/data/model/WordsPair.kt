package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.model

fun List<String>.toWordsPairList(): List<WordsPair> {
    val values = mutableListOf<WordsPair>()
    for(i in 0 until size) {
        for (j in (i+1) until size) {
            val wordsPair = WordsPair(this[i], this[j])
            values.add(wordsPair)
        }
    }
    return values
}

data class WordsPair(
    val value1: String,
    val value2: String
)