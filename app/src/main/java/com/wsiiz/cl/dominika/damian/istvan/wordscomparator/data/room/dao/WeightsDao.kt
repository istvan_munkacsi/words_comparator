package com.wsiiz.cl.dominika.damian.istvan.wordscomparator.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wsiiz.cl.dominika.damian.istvan.wordscomparator.nn.WordComparatorNeuralNetwork
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface WeightsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weights: WordComparatorNeuralNetwork.Weights): Completable

    @Query("SELECT * FROM weights ORDER BY id DESC LIMIT 1")
    fun getLast(): Maybe<WordComparatorNeuralNetwork.Weights>
}